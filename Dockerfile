# Build remco from specific commit
######################################################
FROM golang AS remco

# remco (lightweight configuration management tool) https://github.com/HeavyHorst/remco
RUN go install github.com/HeavyHorst/remco/cmd/remco@latest


# Build avorion base
######################################################
FROM ubuntu:jammy AS avorion-base
SHELL ["/bin/bash", "-o", "pipefail", "-c"]

ENV DEBIAN_FRONTEND noninteractive
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

ENV AVORION_HOME=/home/avorion
ENV AVORION_UID=10000
ENV AVORION_GID=10000

USER root

## Install system packages
RUN dpkg --add-architecture i386
RUN apt-get -y update && apt-get -y upgrade && apt-get -y --no-install-recommends install \
    curl \
    vim \
    ca-certificates \
    gdebi-core \
    libgl1-mesa-dri:i386 \
    libgl1-mesa-glx:i386 \
    gcc \
    g++ \
    iproute2

## Create avorion user and group
RUN groupadd -g $AVORION_GID avorion && \
    useradd -l -s /bin/bash -d $AVORION_HOME -m -u $AVORION_UID -g avorion avorion && \
    passwd -d avorion
# RUN echo "avorion ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/avorion

# Install yq yaml/json parser
RUN curl -Lo /usr/local/bin/yq https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 && \
    chmod guo+x /usr/local/bin/yq


# Build avorion image
######################################################
FROM ubuntu:jammy as avorion

ENV DEBIAN_FRONTEND noninteractive
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8

ENV AVORION_HOME=/home/avorion
ENV AVORION_STEAM_VALIDATE="false"
ENV REMCO_HOME=/etc/remco
ENV STEAMAPPID=565060

ARG CI_COMMIT_AUTHOR
ARG CI_COMMIT_TIMESTAMP
ARG CI_COMMIT_SHA
ARG CI_COMMIT_TAG
ARG CI_PROJECT_URL

LABEL maintainer=$CI_COMMIT_AUTHOR
LABEL author=nathan.snow@mimir-tech.org
LABEL description="Avorion dedicated server with SteamCMD automatic updates and remco auto-config"
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.name="registry.gitlab.com/japtain_cack/avorion-server"
LABEL org.label-schema.description="Avorion dedicated server with SteamCMD automatic updates and remco auto-config"
LABEL org.label-schema.url=$CI_PROJECT_URL
LABEL org.label-schema.vcs-url=$CI_PROJECT_URL
LABEL org.label-schema.docker.cmd="docker run -it -d -v /mnt/avorion/world1/:/home/avorion/server/ -p 7777/udp -p 27015/udp -p 18888/tcp registry.gitlab.com/japtain_cack/avorion-server"
LABEL org.label-schema.vcs-ref=$CI_COMMIT_SHA
LABEL org.label-schema.version=$CI_COMMIT_TAG
LABEL org.label-schema.build-date=$CI_COMMIT_TIMESTAMP

## Update permissions
COPY files/entrypoint.sh ${AVORION_HOME}/
COPY --from=avorion-base /etc/passwd /etc/passwd
COPY --from=avorion-base /etc/group /etc/group
COPY --from=avorion-base /etc/shadow /etc/shadow
COPY --from=avorion-base /usr/bin/curl /usr/bin/curl
COPY --from=avorion-base /usr/bin/vim.basic /usr/bin/vim
COPY --from=avorion-base /usr/share/bug/vim /usr/share/bug/vim
COPY --from=avorion-base /usr/share/lintian/overrides/vim /usr/share/lintian/overrides/vim
COPY --from=avorion-base /etc/ca-certificates/update.d /etc/ca-certificates/update.d
COPY --from=avorion-base /etc/ssl/certs /etc/ssl/certs
COPY --from=avorion-base /usr/sbin/update-ca-certificates /usr/sbin/update-ca-certificates
COPY --from=avorion-base /usr/share/ca-certificates/mozilla /usr/share/ca-certificates/mozilla

COPY --from=avorion-base /usr/share/gdebi /usr/share/gdebi
COPY --from=avorion-base /usr/share/python3/runtime.d /usr/share/python3/runtime.d
COPY --from=avorion-base /usr/bin/gdebi /usr/bin/gdebi
COPY --from=avorion-base /usr/share/bug/libgl1-mesa-dri /usr/share/bug/libgl1-mesa-dri
COPY --from=avorion-base /usr/share/drirc.d /usr/share/drirc.d
COPY --from=avorion-base /usr/share/bug/libgl1-mesa-glx /usr/share/bug/libgl1-mesa-glx
COPY --from=avorion-base /usr/bin/c89-gcc /usr/bin/c89-gcc
COPY --from=avorion-base /usr/bin/c99-gcc /usr/bin/c99-gcc
COPY --from=avorion-base /usr/bin/gcc /usr/bin/gcc
COPY --from=avorion-base /usr/bin/gcc-ar /usr/bin/gcc-ar
COPY --from=avorion-base /usr/bin/gcc-nm /usr/bin/gcc-nm
COPY --from=avorion-base /usr/bin/gcc-ranlib /usr/bin/gcc-ranlib
COPY --from=avorion-base /usr/bin/gcov /usr/bin/gcov
COPY --from=avorion-base /usr/bin/gcov-dump /usr/bin/gcov-dump
COPY --from=avorion-base /usr/bin/gcov-tool /usr/bin/gcov-tool
COPY --from=avorion-base /usr/bin/x86_64-linux-gnu-gcc /usr/bin/x86_64-linux-gnu-gcc
COPY --from=avorion-base /usr/bin/x86_64-linux-gnu-gcc-ar /usr/bin/x86_64-linux-gnu-gcc-ar
COPY --from=avorion-base /usr/bin/x86_64-linux-gnu-gcc-nm /usr/bin/x86_64-linux-gnu-gcc-nm
COPY --from=avorion-base /usr/bin/x86_64-linux-gnu-gcc-ranlib /usr/bin/x86_64-linux-gnu-gcc-ranlib
COPY --from=avorion-base /usr/bin/x86_64-linux-gnu-gcov /usr/bin/x86_64-linux-gnu-gcov
COPY --from=avorion-base /usr/bin/x86_64-linux-gnu-gcov-dump /usr/bin/x86_64-linux-gnu-gcov-dump
COPY --from=avorion-base /usr/bin/x86_64-linux-gnu-gcov-tool /usr/bin/x86_64-linux-gnu-gcov-tool
COPY --from=avorion-base /usr/bin/g++ /usr/bin/g++
COPY --from=avorion-base /usr/bin/x86_64-linux-gnu-g++ /usr/bin/x86_64-linux-gnu-g++
COPY --from=avorion-base /usr/lib /usr/lib

COPY --from=avorion-base /usr/local/bin/yq /usr/local/bin/yq
COPY --from=remco /go/bin/remco /usr/local/bin/remco
COPY --chown=root:root remco /etc/remco
RUN chmod ugo+x ${AVORION_HOME}/entrypoint.sh && \
    chmod -R 0775 /etc/remco && \
    chown -Rv avorion:avorion ${AVORION_HOME}/

WORKDIR ${AVORION_HOME}
VOLUME "${AVORION_HOME}/server"

# server ports
EXPOSE 27000/tcp
EXPOSE 27003/udp
EXPOSE 27020/udp
EXPOSE 27021/tcp

USER avorion
ENTRYPOINT ["./entrypoint.sh"]

