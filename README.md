[![Tag](https://img.shields.io/gitlab/v/tag/japtain_cack/avorion-server?style=for-the-badge)](https://gitlab.com/japtain_cack/avorion-server/-/tags)
[![Gitlab Pipeline](https://img.shields.io/gitlab/pipeline-status/japtain_cack/avorion-server?branch=master&style=for-the-badge)](https://gitlab.com/japtain_cack/avorion-server/-/pipelines)
[![Issues](https://img.shields.io/gitlab/issues/open/japtain_cack/avorion-server?style=for-the-badge)](https://gitlab.com/japtain_cack/avorion-server/-/issues)
[![License](https://img.shields.io/badge/License-CC%20BY--ND%204.0-blue?style=for-the-badge)](https://creativecommons.org/licenses/by-nd/4.0/)


# Avorion Server Docker Container

## Overview
This repository provides a Docker solution for running a Avorion server with automatic updates and configuration management via Remco. It also includes instructions for deploying the server to a Kubernetes cluster using Kustomize.

## Features
- **Automatic Updates**: Ensures the Avorion Server is up-to-date.
- **Remco Configuration Management**: Simplifies environment-specific configurations.
- **Kubernetes Deployment**: Instructions for deploying the server to a Kubernetes cluster using Kustomize.

## Prerequisites
Before deploying the server, ensure:
- The `galaxies` directory is mounted outside the container to preserve data.
- File system permissions are set correctly (e.g., `chown 10000:10000 /mount/path`).
- Environment variables are used to customize the `server.ini` file.
- You have a Kubernetes cluster up and running.
- You have installed and configured Kustomize.

## Quick Start
Launch the server with customized settings using the following command:

```bash
docker run -d -it --name=avorion1 \
  -v /opt/avorion/world1:/home/avorion/server \
  -p 27000:27000/tcp \
  -p 27000:27000/udp \
  -p 27003:27003/udp \
  -p 27020:27020/udp \
  -p 27021:27021/udp \
  -e AVORION_NAME=world1 \
  -e AVORION_DESCRIPTION=avorion.example.com \
  -e AVORION_ISONLINE="true" \
  registry.gitlab.com/japtain_cack/avorion-server:latest
```

## Deploying to Kubernetes using Kustomize
To deploy the Avorion server to a Kubernetes cluster using Kustomize, follow these steps:

1. Ensure you have a Kubernetes cluster up and running.
2. Install and configure Kustomize.
4. Create a new directory somewhere, recommeded to keep this in git, then create the following kustomize files.

avorion/statefulset-patch.yaml:
```yaml
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: avorion-server
spec:
  template:
    spec:
      containers:
      - name: avorion-server
        env:
          - name: AVORION_GALAXYNAME
            value: "avorion.talos.mimir-tech.org"
          - name: AVORION_ADMIN
            value: "76561198002476643"
          - name: AVORION_NAME
            value: "avorian.talos.mimir-tech.org"
          - name: AVORION_DESCRIPTION
            value: "avorian.talos.mimir-tech.org"
          - name: AVORION_ISLISTED
            value: "true"
        resources:
          requests:
            cpu: 1000m
            memory: 2Gi
          limits:
            cpu: null
            memory: null
```

avorion/kustomization.yaml:
```yaml
apiVersion: kustomize.config.k8s.io/v1beta1
kind: Kustomization
namespace: avorion

resources:
  - https://gitlab.com/japtain_cack/avorion-server.git

patches:
  - path: statefulset-patch.yaml
    target:
      kind: StatefulSet
      name: avorion-server

images:
  - name: registry.gitlab.com/japtain_cack/avorion-server
    newname: registry.gitlab.com/japtain_cack/avorion-server
    newTag: "1"
```

5. Apply the Kustomize configuration to your Kubernetes cluster:

```bash
kubectl apply -k avorion-server
```

This will create a StatefulSet and a Service in your Kubernetes cluster, and your Avorion server will be up and running. If you don't use ExternalSecrets,
just make a normal kubernetes secret. However, don't commit secrets to the repo for security reasons.

## Management Commands
- **Stop and Remove Containers**: `docker kill $(docker ps -qa); docker rm $(docker ps -qa)`
- **View Logs**: `docker logs avorion1`
- **Attach to Console**: `docker attach avorion1` (Detach with `CTRL+p` + `CTRL+q`)
- **Access Bash Console**: `docker exec -it avorion1 bash`

> **Note**: Container names are referenced if the `--name` flag is used in the `docker run` command.

## SELinux Context for Volumes
Set the SELinux context for mounted volumes with:
`chcon -Rt svirt_sandbox_file_t /path/to/volume`

## Configuration
- **UID/GID**: Optionally set with `AVORION_UID` and `AVORION_GID`.
- **Environment Variables**: Refer to [server.ini template](https://gitlab.com/japtain_cack/avorion-server/-/blob/master/remco/templates/server.ini) for all variables.

## Remco Template Keys
Remco templates use keys that map to environment variables, transforming `/avorion/some-option` to `AVORION_SOME_OPTION`. The `getv()` function in the template sets default values:

```bash
getv("/avorion/some-option", "default-value")
```

To override a default value, use the `-e` flag:

```bash
docker run -e AVORION_SOME_OPTION=my-value ...
```

For detailed documentation, visit [Remco's GitHub repository](https://github.com/HeavyHorst/remco).
