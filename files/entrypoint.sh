#!/usr/bin/env bash

export REMCO_RESOURCE_DIR=${REMCO_HOME}/resources.d
export REMCO_TEMPLATE_DIR=${REMCO_HOME}/templates
export LD_LIBRARY_PATH=/home/avorion/server/linux64:/home/avorion/server/linux32:/home/avorion/server/steamcmd/linux64:/home/avorion/server/steamcmd/linux32:/usr/lib/x86_64-linux-gnu:$LD_LIBRARY_PATH

echo "#####################################"
date
echo

chown -Rv avorion:avorion ${AVORION_HOME}

if [[ ! -z $AVORION_STEAM_VALIDATE ]]; then
  if [[ ! "$AVORION_STEAM_VALIDATE" =~ true|false ]]; then
    echo '[ERROR] AVORION_STEAM_VALIDATE must be true or false'
    exit 1
  elif [[ "$AVORION_STEAM_VALIDATE" == true ]]; then
    AVORION_STEAM_VALIDATE_VALUE="validate"
  else
    AVORION_STEAM_VALIDATE_VALUE=""
  fi
fi

## Install SteamCMD
mkdir -p ${AVORION_HOME}/server/steamcmd
mkdir -p ${AVORION_HOME}/server/.backup
curl -sqL "https://steamcdn-a.akamaihd.net/client/installer/steamcmd_linux.tar.gz" | tar zxvf - -C ${AVORION_HOME}/server/steamcmd/
chmod ugo+x ${AVORION_HOME}/server/steamcmd/steamcmd.sh

cat <<EOF> ${AVORION_HOME}/server/steamcmd/avorion.conf
@ShutdownOnFailedCommand 1
@NoPromptForPassword 1
@sSteamCmdForcePlatformType linux
force_install_dir ${AVORION_HOME}/server/
login anonymous
app_update ${STEAMAPPID} ${AVORION_STEAM_VALIDATE_VALUE}
quit
EOF

cd ${AVORION_HOME}/server/
steamcmd/steamcmd.sh +runscript ${AVORION_HOME}/server/steamcmd/avorion.conf

echo
echo "#####################################"
echo Generating configs...
echo
remco

echo
echo "#####################################"
echo starting server...
echo

./start.sh

