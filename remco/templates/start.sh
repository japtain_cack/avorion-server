./server.sh \
  --galaxy-name='{{ getv("/avorion/galaxyname", "galaxy") }}' \
  --admin='{{ getv("/avorion/admin", "avorion-admin-steamid") }}' \
{% if getv("/avorion/trace", "false") == "true" %}
  --trace all \
{% endif %}
  --datapath='{{ getv("/avorion/datapath", "/home/avorion/server/galaxies") }}'
